<?php

namespace App\Form;

use App\Entity\PropertySearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProviderPropertySearch extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('region', ChoiceType::class, [
                'choices' => [
                    'Ariana'      => 'Ariana',
                    'Béja'        => 'Béja',
                    'Ben Arous'   => 'Ben Arous',
                    'Bizerte'     => 'Bizerte',
                    'Gabès'       => 'Gabès',
                    'Gafsa'       => 'Gafsa',
                    'Jendouba'    => 'Jendouba',
                    'Kairouan'    => 'Kairouan',
                    'Kasserine'   => 'Kasserine',
                    'Kébili'      => 'Kébili',
                    'Kef'         => 'Kef',
                    'Mahdia'      => 'Mahdia',
                    'Mannouba'    => 'Mannouba',
                    'Médennine'   => 'Médennine',
                    'Monastir'    => 'Monastir',
                    'Nabeul'      => 'Nabeul',
                    'Sfax'        => 'Sfax',
                    'Sidi Bouzid' => 'Sidi Bouzid',
                    'Siliana'     => 'Siliana',
                    'Sousse'      => 'Sousse',
                    'Tataouine'   => 'Tataouine',
                    'Tozeur'      => 'Tozeur',
                    'Tunis'       => 'Tunis',
                    'Zaghouan'    => 'Zaghouan',
                ],
                'placeholder' => 'Région',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertySearch::class,
            "method" => "get",
            "csrf_protection" => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return "";
    }
}
