<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;


class UserMyProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('email')
            ->add('address')
            ->add('vat_number')
            ->add('phone')
            ->add('field')
            ->add('regions', ChoiceType::class, [
                'choices' => [
                    'Ariana'      => 'Ariana',
                    'Béja'        => 'Béja',
                    'Ben Arous'   => 'Ben Arous',
                    'Bizerte'     => 'Bizerte',
                    'Gabès'       => 'Gabès',
                    'Gafsa'       => 'Gafsa',
                    'Jendouba'    => 'Jendouba',
                    'Kairouan'    => 'Kairouan',
                    'Kasserine'   => 'Kasserine',
                    'Kébili'      => 'Kébili',
                    'Kef'         => 'Kef',
                    'Mahdia'      => 'Mahdia',
                    'Mannouba'    => 'Mannouba',
                    'Médennine'   => 'Médennine',
                    'Monastir'    => 'Monastir',
                    'Nabeul'      => 'Nabeul',
                    'Sfax'        => 'Sfax',
                    'Sidi Bouzid' => 'Sidi Bouzid',
                    'Siliana'     => 'Siliana',
                    'Sousse'      => 'Sousse',
                    'Tataouine'   => 'Tataouine',
                    'Tozeur'      => 'Tozeur',
                    'Tunis'       => 'Tunis',
                    'Zaghouan'    => 'Zaghouan',
                ],
                'placeholder' => '',
                'multiple' => true
            ])
            ->add('logo', FileType::class, [
                'data_class' => null,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
