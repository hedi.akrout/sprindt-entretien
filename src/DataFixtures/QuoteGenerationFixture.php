<?php

namespace App\DataFixtures;

use App\Entity\QuoteGeneration;
use App\Entity\Ticket;
use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class QuoteGenerationFixture extends Fixture
{
    private $userRepository;
    private $companyRepository;

    public function __construct(UserRepository $userRepository, CompanyRepository $companyRepository)
    {
        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
    }

    public function load(ObjectManager $manager)
    {
        $client   = $this->userRepository->find(5);
        $provider = $this->userRepository->find(2);
        $company = $this->companyRepository->find(2);

        $html = '<table width="100%" class="nbd"></table>';

        $year = date('Y');
        $day  = date('d');

        for ($i = 1; $i <= 12; $i++) {
            echo "Generation from month $i \n";
            $date = new \DateTime("$year-$i-$day");
            $generationsCount = mt_rand(20, 500);
            $amount = mt_rand(500, 2000);
            for ($j = 0; $j < $generationsCount; $j++) {
                $isOrdered = rand(0, 1) == 1;

                $quoteGeneration = new QuoteGeneration();
                $quoteGeneration
                    ->setClient($client)
                    ->setProvider($provider)
                    ->setCompany($company)
                    ->setCreatedAt($date)
                    ->setHtml($html)
                    ->setIsOrdered($isOrdered)
                    ->setAmount($amount)
                ;
                $manager->persist($quoteGeneration);

                if ($isOrdered) {
                    $ticket = new Ticket();
                    $ticket
                        ->setCreatedAt($date)
                        ->setHtmlQuote($html)
                        ->setMessage("Lorem Ipsum dolor site amet")
                        ->setState(0)
                        ->setClient($client)
                        ->setProvider($provider)
                    ;
                    $manager->persist($ticket);

                    $quoteGeneration->setTicket($ticket);
                    $manager->persist($quoteGeneration);
                }
            }
        }

        $manager->flush();
    }
}
