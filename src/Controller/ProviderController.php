<?php
namespace App\Controller;

use App\Entity\Product;
use App\Entity\PropertySearch;
use App\Entity\User;
use App\Form\ProviderPropertySearch;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/fournisseurs")
 */
class ProviderController extends AbstractController
{

    /**
     * @Route("/", name="front_provider_listing")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, UserRepository $userRepository,  PaginatorInterface $paginator)
    {
        $propertySearch = new PropertySearch();
        $form = $this->createForm(ProviderPropertySearch::class, $propertySearch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $users = $userRepository->findBySearch($propertySearch);
        } else {
            $users = $userRepository->findBy(["account_type" => "ROLE_PROVIDER", "is_subscribed" => true]);
        }

        $users = $paginator->paginate(
            $users,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render("provider/index.html.twig", [
            "users" => $users,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/{slug}", name="front_provider_products_listing")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param $slug
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function productsListing(Request $request, UserRepository $userRepository, $slug,  PaginatorInterface $paginator)
    {
        $user = $userRepository->findOneBy(['slug' => $slug]);
        if (!$user || !$user->isSubscribed()) {
            return $this->redirectToRoute('front_home');
        }
        $products = $user->getProducts();
        $products = $paginator->paginate(
            $products,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render("provider/products_listing.html.twig", [
            "products" => $products,
            "user" => $user
        ]);
    }

    /**
     * @Route("/{provider}/{slug}-{id}", name="front_provider_product_show", requirements={"slug": "[a-z0-9\-]*"})
     * @param $slug
     * @param Product $product
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function productShow($slug, Product $product, ProductRepository $productRepository)
    {
        if ($product->getSlug() !== $slug) {
            return $this->redirectToRoute("front_provider_product_show", [
                "id" => $product->getId(),
                "slug" => $product->getSlug(),
                "provider" => $product->getUser()->getSlug()
            ], 301);
        }

        $related_products = $productRepository->getRelatedProducts($product);

        return $this->render("provider/product_show.html.twig", [
            "product" => $product,
            "related_products" => $related_products,
            "user" => $product->getUser(),
        ]);
    }
}