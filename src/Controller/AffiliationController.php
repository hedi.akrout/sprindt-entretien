<?php
namespace App\Controller;

use App\Entity\Affiliation;
use App\Repository\AffiliationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @isGranted("ROLE_ADMIN")
 * @Route("/dashboard/affilition")
 */
class AffiliationController extends AbstractController
{

    /**
     * @Route("/", name="dashboard_affiliation_index")
     * @param Request $request
     * @param AffiliationRepository $affiliationRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, AffiliationRepository $affiliationRepository, PaginatorInterface $paginator): Response
    {
        $affiliations = $affiliationRepository->findBy([], ["state" => "DESC"]);
        $affiliations = $paginator->paginate(
            $affiliations,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('affiliation/index.html.twig', [
            'affiliations' => $affiliations
        ]);
    }

    /**
     * @Route("/{id}", name="dashboard_affiliation_show")
     * @param Affiliation $affiliation
     * @return Response
     */
    public function show(Affiliation $affiliation): Response
    {
        $is_new = false;
        if (!$affiliation->getState()) {
            $is_new = true;
            $affiliation->setState(true);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render("affiliation/show.html.twig", [
            "affiliation" => $affiliation,
            "is_new" => $is_new
        ]);
    }
}