<?php

namespace App\Controller;

use App\Entity\Field;
use App\Form\FieldType;
use App\Repository\FieldRepository;
use Cocur\Slugify\Slugify;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard/field")
 */
class FieldController extends AbstractController
{
    /**
     * @Route("/", name="dashboard_field_index", methods={"GET", "POST"})
     * @param FieldRepository $fieldRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(FieldRepository $fieldRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $field = new Field();
        $form = $this->createForm(FieldType::class, $field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $field->setSlug($slugify->slugify($field->getName()));

            $logoFile = $form->get('image')->getData();
            if ($logoFile) {
                $path = $this->getParameter('fields_directory');
                $originalFilename = pathinfo($logoFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logoFile->guessExtension();
                try {
                    $logoFile->move(
                        $path,
                        $newFilename
                    );
                } catch (FileException $e) {
                    $newFilename = "Error occured during uplaod";
                }
                $field->setImage($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($field);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard_field_index');
        }

        $fields = $fieldRepository->findAll();
        $fields = $paginator->paginate(
            $fields,
            $request->query->getInt('page', 1),
            7
        );

        return $this->render('field/index.html.twig', [
            'fields' => $fields,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="dashboard_field_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $field = new Field();
        $form = $this->createForm(FieldType::class, $field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($field);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard_field_index');
        }

        return $this->render('field/new.html.twig', [
            'field' => $field,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dashboard_field_show", methods={"GET"})
     */
    public function show(Field $field): Response
    {
        return $this->render('field/show.html.twig', [
            'field' => $field,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="dashboard_field_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Field $field
     * @param FieldRepository $fieldRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function edit(Request $request, Field $field, FieldRepository $fieldRepository, PaginatorInterface $paginator): Response
    {
        $originalLogo = $field->getImage();
        $form = $this->createForm(FieldType::class, $field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logoFile = $form->get('image')->getData();
            if ($logoFile) {
                $path = $this->getParameter('logos_directory');
                $originalFilename = pathinfo($logoFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logoFile->guessExtension();
                try {
                    $logoFile->move(
                        $path,
                        $newFilename
                    );
                } catch (FileException $e) {
                    $newFilename = "Error occured during uplaod";
                }
                $field->setImage($newFilename);
            } else {
                $field->setImage($originalLogo);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dashboard_field_index');
        }

        $fields = $fieldRepository->findAll();
        $fields = $paginator->paginate(
            $fields,
            $request->query->getInt('page', 1),
            7
        );

        return $this->render('field/index.html.twig', [
            'field' => $field,
            'fields' => $fields,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dashboard_field_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Field $field): Response
    {
        if ($this->isCsrfTokenValid('delete'.$field->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($field);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dashboard_field_index');
    }

    /**
     * @Route("/{id}/delete", name="dashboard_field_confirm_delete", methods={"GET","POST"})
     * @param Request $request
     * @param Field $field
     * @return Response
     */
    public function confirmDelete(Request $request, Field $field): Response
    {

        return $this->render('field/confirm_delete.html.twig', [
            'field' => $field,
        ]);
    }
}
