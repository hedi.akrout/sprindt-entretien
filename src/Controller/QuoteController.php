<?php
namespace App\Controller;

use App\Entity\Company;
use App\Entity\Notification;
use App\Entity\Product;
use App\Entity\QuoteGeneration;
use App\Entity\QuoteItem;
use App\Entity\Ticket;
use App\Entity\User;
use App\Form\TicketType;
use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuoteController extends AbstractController
{

    /**
     * @Route("/add-token-spoqze", name="set_redirect_system_after_login", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function setRedirectSystemAfterLogin(Request $request)
    {
        $session = $request->getSession();
        $session->set('authentication_before_quote_generation', true);
        return new Response('ok', 200);
    }

    /**
     * @Route("/generation-de-devis/choix-des-entreprises", name="front_company_selection")
     * @param Request $request
     * @return Response
     */
    public function selectCompanies(Request $request): Response
    {
        $session = $request->getSession();
        $cart = $session->get('cart', false);
        if (!$cart) {
            return $this->redirectToRoute("front_fields_index");
        }

        $user = $this->getUser();
        $form = $this->createFormBuilder()
            ->add("quoteFor", ChoiceType::class, [
                "choices" => [
                    "Pour moi" => 1,
                    "Pour mon entreprise" => 2
                ],
                "label" => "Devis généré pour ?"
            ])
            ->add("company", EntityType::class, [
                "class" => Company::class,
                "query_builder" => function(CompanyRepository $er) use ($user) {
                    return $er->createQueryBuilder('c')
                        ->where('c.user = :user')
                        ->setParameter('user', $user);
                },
                "label" => "Entreprise"
            ])
            ->getForm()
        ;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = $request->getSession();
            if ($form->getData()["quoteFor"] === 2) {
                $session->set('company', $form->getData()["company"]->getId());
            } else {
                $session->set('company', -1);
            }

            return $this->redirectToRoute("front_quote_generation");
        }

        return $this->render("quote/company_selection.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/generation-de-devis", name="front_quote_generation")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param CompanyRepository $companyRepository
     * @return Response
     */
    public function generate(Request $request, UserRepository $userRepository, CompanyRepository $companyRepository)
    {
        $session = $request->getSession();
        $companyId = (int) $session->get('company', false);
        if (!$companyId) {
            return $this->redirectToRoute("front_company_selection");
        }

        $company = null;
        if ($companyId != -1) {
            $company = $companyRepository->find($companyId);
            if (!$company) {
                return $this->redirectToRoute("front_company_selection");
            }
        }

        $em = $this->getDoctrine()->getManager();
        $ticket = new Ticket();
        $form = $this->createForm(TicketType::class, $ticket);

        $cart = $session->get('cart', false);
        $providerSlug = $session->get('provider', false);
        $provider = $userRepository->findOneBy(['slug' => $providerSlug]);

        /** @var User $client */
        $client = $this->getUser();

        $quoteHtml = '';

        if ($cart) {
            if ($company) {
                $quoteHtml = $this->getQuoteHtml($cart, $provider, $company);
            } else {
                $quoteHtml = $this->getQuoteHtml($cart, $provider);
            }

            $generation = new QuoteGeneration();
            $generation->setProvider($provider);
            $generation->setClient($client);
            $generation->setCompany($company);
            $generation->setHtml($quoteHtml);

            $em->persist($generation);

            $amount = 0;
            foreach ($cart as $item) {
                /** @var Product $product */
                $product = $em->getRepository(Product::class)->findOneBy(["sp_sku" => $item->id]);
                if ($product) {
                    $quoteItem = new QuoteItem();
                    $quoteItem->setQuoteGeneration($generation);
                    $quoteItem->setProduct($product);
                    $quoteItem->setUnitPrice($product->getPrice());
                    $quoteItem->setQuantity($item->qty);

                    $em->persist($quoteItem);
                    $amount += $product->getPrice();
                }
            }

            $generation->setAmount($amount);
            $em->flush();

            $session->remove('cart');
            $session->remove('provider');

            $form->add('generation_id', HiddenType::class, [
                'data' => $generation->getId(),
                'mapped' => false
            ]);
        } else {
            $form->add('generation_id', HiddenType::class, [
                'mapped' => false
            ]);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $session->remove('company');
            $data = $form->all();
            $generation = $em->getRepository(QuoteGeneration::class)->find($data['generation_id']->getData());

            if ($generation) {
                $client = $generation->getClient();
                $provider = $generation->getProvider();

                $ticket->setQuoteGeneration($generation);
                $ticket->setClient($client);
                $ticket->setProvider($provider);
                $ticket->setHtmlQuote($generation->getHtml());

                $em->persist($ticket);

                $generation->setIsOrdered(true);
                $generation->setTicket($ticket);

                $notification = new Notification();
                $notification->setUser($provider);
                $notification->setType(1);
                $em->persist($notification);

                $em->flush();
                $this->addFlash('success', 'Votre ticket a été créé et vous serez contacté par un commercial dans les plus brefs délais !');
            }

            return $this->redirectToRoute('front_user_tickets');
        }

        return $this->render('quote/generate.html.twig', [
            'quoteHTML' => $quoteHtml,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/generation-de-devis-pdf", name="front_quote_generation_pdf")
     * @return Response
     */
    public function generateQuotePdf()
    {
        $container = $this->container;
        $pdf = $container->get("white_october.tcpdf")->create();

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetFont('helvetica', '', 9, '', true);
        $pdf->AddPage('P', 'A4');

        $html = "";

        $pdf->writeHTML($html);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $pdf->Output("facture--".".pdf", 'I');
        return $response;
    }

    private function getQuoteHtml($cart, User $provider, Company $company = null)
    {
        $em = $this->getDoctrine()->getManager();
        $html = '';
        $total = 0;
        $user = $this->getUser();

        if ($company) {
            $clientName = $company->getName();
            $clientVat = $company->getVatNumber();
            $clientAddress = $company->getAddress();
        } else {
            $clientName = $user->getName();
            $clientVat = "-";
            $clientAddress = $user->getAddress();
        }

        if ($cart != null) {

            // Header
            $imgSprindt = '/style/images/logo.png';
            $img = $provider->getLogoUrl();
            $date = new \DateTime();
            $date = $date->format('d/m/Y');

            $html .= '
            <table width="100%" class="nbd">
                <tr>
                    <td valign="middle" align="left">
                        <br><br>
                        <img src="' . $img . '" width="100" alt="logo" class="logo-provider">
                    </td>
                    <td valign="middle" align="right">
                        <table width="100%" class="nbd">
                            <tr>
                                <td valign="middle" align="right">
                                    <strong>Date</strong> : ' . $date . '
                                    <br>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="1" width="100%" cellpadding="5" class="bordered-custom-table">
                                        <tr>
                                            <td align="left">Nom</td>
                                            <td align="center">' . $provider->getName() . '</td>
                                        </tr>
                                        <tr>
                                            <td align="left">M.Fiscale</td>
                                            <td align="center">' . $provider->getVatNumber() . '</td>
                                        </tr>
                                        <tr>
                                            <td align="left">Adresse</td>
                                            <td align="center">10 Rue St Augustin, Tunis 1082</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td valign="middle" align="right">
                        <br><br><br>
                        <h2 style="text-align: center">À l\'attention de</h2>
                        <br>
                        <table border="1" width="100%" cellpadding="5" class="bordered-custom-table">
                            <tr>
                                <td align="left">Nom/Raison sociale</td>
                                <td align="center">' . $clientName . '</td>
                            </tr>
                            <tr>
                                <td align="left">M.Fiscale</td>
                                <td align="center">' . $clientVat . '</td>
                            </tr>
                            <tr>
                                <td align="left">Adresse</td>
                                <td align="center">' . $clientAddress . '</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br><br><br><br><br>
        ';

            $html .= '
            <table width="100%" class="nbd">
                <tr>
                    <td align="center">
                        <h2>Prototype de devis</h2>
                        <br>
                    </td>
                </tr>
            </table>';

            $html .= '<table border="1" width="100%" cellpadding="5" class="bordered-custom-table">';
            $html .= '
                <tr>
                    <td width="55%" valign="middle" align="left"><strong>DESCRIPTION</strong></td>
                    <td width="15%" valign="middle" align="right"><strong>QUANTITE</strong></td>
                    <td width="15%" valign="middle" align="right"><strong>P.U HT</strong></td>
                    <td width="15%" valign="middle" align="right"><strong>MONTANT HT</strong></td>
                </tr>
        ';

            foreach ($cart as $item) {
                $product = $em->getRepository(Product::class)->findOneBy(["sp_sku" => $item->id]);
                $html .= '
                    <tr>
                        <td valign="top" align="left">' . $product->getDescription() . '</td>
                        <td valign="top" align="right">
                            ' . $item->qty . '
                            <br><br><br><br><br><br><br><br>
                        </td>
                        <td valign="top" align="right">' . $product->getPrice() . '</td>';
                $productTotal = $product->getPrice() * $item->qty;
                $total += $productTotal;
                $html .= '<td valign="top" align="right">' . $productTotal . '</td>
                    </tr>
                ';
            }

            $html .= '</table>';

            $html .= '<table style="margin-top: -2px;" width="100.1%" class="nbd np">';
            $html .=
                '<tr>
                <td width="54.5%" class="td-w-55"></td>
                <td width="45%" class="td-n-p">
                    <table border="1" width="100%" cellpadding="5" class="total-table">
                        <tr>
                            <td width="66.7%" valign="middle" align="left">Sous-total</td>
                            <td width="33.3%" valign="middle" align="right">' . $total . '</td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left">Total TTC</td>
                            <td valign="middle" align="right">' . $total * 1.19 . '</td>
                        </tr>
                    </table>
                </td>
            </tr>
            ';

            $html .= '</table>';

            //footer
            $html .= '
            <br><br>
            <table width="100%" class="nbd">
                <tr>
                    <td align="center">
                        <br><br>
                        <strong>MERCI DE LA CONFIANCE QUE VOUS NOUS TÉMOIGNEZ</strong>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <br><br>
                        <strong>Signature</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br><br><br><br><br><br><br>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <hr>
                        <br><br>
                        Devis généré par la plateforme Sprindt<br>
                        <img style="width: 70px;" src="' . $imgSprindt . '" alt="" width="70">
                    </td>
                </tr>
            </table>
        ';
        }

        return $html;
    }
}