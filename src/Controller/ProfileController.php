<?php
namespace App\Controller;

use App\Entity\Company;
use App\Entity\Notification;
use App\Entity\Product;
use App\Entity\QuoteGeneration;
use App\Entity\Ticket;
use App\Entity\User;
use App\Form\CompanyType;
use App\Form\ProfileType;
use App\Form\TicketType;
use App\Repository\ProductRepository;
use App\Repository\QuoteGenerationRepository;
use App\Repository\TicketRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("mon-compte")
 */
class ProfileController extends AbstractController
{

    /**
     * @Route("/", name="front_user_account")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user=$this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newPassword = $form->get('newPassword')->getData();
            if ($newPassword != null){
                $user->setPassword(
                    $passwordEncoder->encodePassword($user, $newPassword)
                );
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Votre profil a été modifié");
        }

        $this->getDoctrine()->getManager()->refresh($user);
        return $this->render('profile/index.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/mes-tickets", name="front_user_tickets")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param TicketRepository $ticketRepository
     * @return Response
     */
    public function tickets(Request $request, PaginatorInterface $paginator, TicketRepository $ticketRepository)
    {
        $tickets = $ticketRepository->findBy(["client" => $this->getUser()], ["created_at" => "DESC"]);
        $tickets = $paginator->paginate(
            $tickets,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('profile/tickets.html.twig', [
            'tickets' => $tickets
        ]);
    }

    /**
     * @Route("/mes-devis-generes", name="front_user_generations")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param QuoteGenerationRepository $generationRepository
     * @return Response
     */
    public function generations(Request $request, PaginatorInterface $paginator, QuoteGenerationRepository $generationRepository)
    {
        $generations = $generationRepository->findBy(["client" => $this->getUser()], ["created_at" => "DESC"]);
        $generations = $paginator->paginate(
            $generations,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('profile/generations.html.twig', [
            'generations' => $generations
        ]);
    }

    /**
     * @Route("/devis/{id}", name="front_generation_show")
     * @param Request $request
     * @param QuoteGeneration $generation
     * @return Response
     */
    public function generationShow(Request $request, QuoteGeneration $generation)
    {
        return $this->render('profile/generation_show.html.twig', [
            'generation' => $generation
        ]);
    }

    /**
     * @Route("/ticket/{id}", name="front_ticket_show")
     * @param Request $request
     * @param Ticket $ticket
     * @return Response
     */
    public function ticketShow(Request $request, Ticket $ticket)
    {
        return $this->render('profile/ticket_show.html.twig', [
            'ticket' => $ticket
        ]);
    }

    /**
     * @Route("/demande-achat/{id}", name="front_quote_order")
     * @param Request $request
     * @param QuoteGeneration $generation
     * @return Response
     */
    public function order(Request $request, QuoteGeneration $generation)
    {
        $ticket = new Ticket();
        $form = $this->createForm(TicketType::class, $ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ticket->setQuoteGeneration($generation);
            $ticket->setClient($generation->getClient());
            $ticket->setProvider($generation->getProvider());
            $ticket->setHtmlQuote($generation->getHtml());

            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);

            $notification = new Notification();
            $notification->setUser($generation->getProvider());
            $notification->setType(1);
            $em->persist($notification);

            $generation->setIsOrdered(true);
            $generation->setTicket($ticket);
            $em->flush();

            $this->addFlash('success', 'Votre ticket a été créé et vous serez contacté par un commercial dans les plus brefs délais !');
            return $this->redirectToRoute('front_user_tickets');
        }

        return $this->render('profile/order.html.twig', [
            'generation' => $generation,
            'form' => $form->createView()
        ]);
    }




    /**
     * @Route("/mes-entreprises", name="front_user_company")
     * @param Request $request
     * @return Response
     */
    public function myCompanies(Request $request)
    {
        $companies = $this->getUser()->getCompanies();

        return $this->render('profile/companies.html.twig', [
            'companies' => $companies
        ]);
    }

    /**
     * @Route("/mes-entreprises/{id}", name="front_company_show")
     * @param Request $request
     * @param Company $company
     * @return Response
     */
    public function companyShow(Request $request, Company $company)
    {
        $user = $this->getUser();
        if ($user->getId() != $company->getUser()->getId()) {
            return $this->redirectToRoute('front_home');
        }
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute("front_user_company");
        }
        return $this->render('profile/company_new.html.twig', [
            'company' => $company,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/ajouter-une-entreprise", name="front_company_new")
     * @param Request $request
     * @return Response
     */
    public function companyNew(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $companies = $user->getCompanies();
        if (count($companies) >= 3) {
            $this->addFlash("succes", "Nombre maximale des entrprises atteint");
            return $this->redirectToRoute("front_user_company");
        }

        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $company->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute("front_user_company");
        }

        return $this->render('profile/company_new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}