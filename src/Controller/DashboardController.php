<?php
namespace App\Controller;

use App\Entity\User;
use App\Repository\ProductRepository;
use App\Repository\QuoteGenerationRepository;
use App\Repository\TicketRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 * @package App\Controller
 * @Route("/dashboard")
 */
class DashboardController extends AbstractController
{

    /**
     * @Route("/", name="dashboard_index")
     * @param UserRepository $userRepository
     * @param ProductRepository $productRepository
     * @param QuoteGenerationRepository $generationRepository
     * @param TicketRepository $ticketRepository
     * @return Response
     */
    public function index(UserRepository $userRepository, ProductRepository $productRepository, QuoteGenerationRepository $generationRepository, TicketRepository $ticketRepository)
    {
        /** @var User $user */
        $user = $this->getUser();

        $generationCount = $generationRepository->getMonthCount();
        $ticketCount = $ticketRepository->getMonthCount();

        if ($this->isGranted('ROLE_ADMIN')) {
            $tickets = $ticketRepository->findBy([], ["id" => "desc"], 5);
            $quote_generations = $generationRepository->findBy([], ["id" => "desc"], 5);
            $productsCount = $productRepository->findCount();
            $amounts = $generationRepository->getMonthAmount();
            $ticketStates = $ticketRepository->getMonthState();

            $providersCount = $userRepository->getUsersCountByAccountType('ROLE_PROVIDER');
            $clientsCount = $userRepository->getUsersCountByAccountType('ROLE_CLIENT');

            $volume = $generationRepository->getMonthVolume();
        } else {
            $tickets = $ticketRepository->findBy(['provider' => $user], ["id" => "desc"], 5);
            $quote_generations = $generationRepository->findBy(['provider' => $user], ["id" => "desc"], 5);
            $productsCount = $productRepository->findCount($user);
            $amounts = $generationRepository->getMonthAmount($user);
            $ticketStates = $ticketRepository->getMonthState($user);

            $providersCount = null;
            $clientsCount = null;

            $volume = $generationRepository->getMonthVolume($user);
        }

        $yearOfJoin = (int) $user->getCreatedAt()->format('Y');
        $currentYear = (int) date('Y');

        return $this->render("dashboard/index.html.twig", [
            'volume' => $volume,
            'generationCount' => $generationCount,
            'ticketCount' => $ticketCount,
            'amounts' => $amounts,
            'productsCount' => $productsCount,
            'providersCount' => $providersCount,
            'clientsCount' => $clientsCount,
            'tickets' => $tickets,
            'quote_generations' => $quote_generations,
            'ticketStates' => $ticketStates,

            'yearOfJoin' => $yearOfJoin,
            'currentYear' => $currentYear,
        ]);
    }

    /**
     * @Route("/yearly-graph-volume", name="dashboard_yearly_graph_volume")
     * @param Request $request
     * @param QuoteGenerationRepository $generationRepository
     * @return Response
     */
    public function yearlyGraphVolume(Request $request, QuoteGenerationRepository $generationRepository): Response
    {
        $data = $request->request->all();
        $year = (int) $data['year'];

        if ($this->isGranted("ROLE_PROVIDER")) {
            $user = $this->getUser();
        } else {
            $user = null;
        }

        $volume = $generationRepository->getMonthVolume($user, $year);
        return new Response(json_encode($volume), 200);
    }

    /**
     * @Route("/chart-data-amount", name="dashboard_chart_data_amount")
     * @param Request $request
     * @param QuoteGenerationRepository $generationRepository
     * @return Response
     * @throws \Exception
     */
    public function getChartDataAmount(Request $request, QuoteGenerationRepository $generationRepository)
    {
        $data = $request->request->all();
        $from = $data['start'];
        $to = $data['end'];

        $from = (new \DateTime($from))->format('Y-m-d');
        $to = (new \DateTime($to))->format('Y-m-d');

        if ($this->isGranted("ROLE_PROVIDER")) {
            /** @var User $user */
            $user = $this->getUser();
            $volume = $generationRepository->getMonthAmount($user, $from, $to);
        } else {
            $volume = $generationRepository->getMonthAmount(null, $from, $to);
        }

        return new Response(json_encode($volume), 200);
    }

    /**
     * @Route("/chart-data-tickets", name="dashboard_chart_data_tickets")
     * @param Request $request
     * @param TicketRepository $ticketRepository
     * @return Response
     * @throws \Exception
     */
    public function getChartDataTickets(Request $request, TicketRepository $ticketRepository)
    {
        $data = $request->request->all();
        $from = $data['start'];
        $to = $data['end'];

        $from = (new \DateTime($from))->format('Y-m-d');
        $to = (new \DateTime($to))->format('Y-m-d');

        if ($this->isGranted("ROLE_PROVIDER")) {
            /** @var User $user */
            $user = $this->getUser();
            $volume = $ticketRepository->getMonthState($user, $from, $to);
        } else {
            $volume = $ticketRepository->getMonthState(null, $from, $to);
        }

        return new Response(json_encode($volume), 200);
    }
}