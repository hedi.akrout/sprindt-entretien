<?php
namespace App\Controller;

use App\Entity\Field;
use App\Entity\Product;
use App\Form\ProductFieldsType;
use App\Form\ProductImportType;
use App\Repository\FieldRepository;
use App\Repository\ProductRepository;
use Cocur\Slugify\Slugify;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard/import-product")
 */
class ProductImportController extends AbstractController
{
    /**
     * @Route("/", name="dashboard_product_import_index", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(ProductImportType::class);
        $form->handleRequest($request);

        return $this->render('product_import/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/choose-columns", name="dashboard_product_import_choose_columns", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function chooseColumns(Request $request): Response
    {
        $fileForm = $this->createForm(ProductImportType::class);
        $fileForm->handleRequest($request);

        $keys = [];
        if ($fileForm->isSubmitted() && $fileForm->isValid()) {
            $file = $fileForm->get('file')->getData();
            $pathName = $file->getPathName();
            $csv = array_map("str_getcsv", file($pathName,FILE_SKIP_EMPTY_LINES));
            $keys = array_shift($csv);
            $keys = array_flip($keys);

            $session = $request->getSession();
            $session->set('csv', $csv);
            // $session->set('keys', $keys);
        }

        $form = $this->createForm(ProductFieldsType::class);
        $form->add('name', ChoiceType::class, [
            "choices" => $keys,
            "required" => false
        ]);
        $form->add('price', ChoiceType::class, [
            "choices" => $keys,
            "required" => false
        ]);
        $form->add('description', ChoiceType::class, [
            "choices" => $keys,
            "required" => false
        ]);
        $form->add('sku', ChoiceType::class, [
            "choices" => $keys,
            "required" => false
        ]);
        $form->add('quantity', ChoiceType::class, [
            "choices" => $keys,
            "required" => false
        ]);
        $form->add('categories', ChoiceType::class, [
            "choices" => $keys,
            "required" => false
        ]);
        $form->add('images', ChoiceType::class, [
            "choices" => $keys,
            'placeholder' => ""
        ]);

        return $this->render('product_import/choose_columns.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/start-import", name="dashboard_product_import_start_import", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function startImport(Request $request): Response
    {
        $form = $this->createForm(ProductFieldsType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $data = $form->getExtraData();
            $session = $request->getSession();
            $session->set('data', $data);
        }

        return $this->render('product_import/start_import.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/start-import-js", name="dashboard_product_import_start_import_js", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function startImportJS(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $csv = $session->get('csv', null);
        $data = $session->get('data', null);

        define('__NAME__', $data['name']);
        define('__PRICE__', $data['price']);
        define('__DESCRIPTION__', $data['description']);
        define('__SKU__', $data['sku']);
        define('__QUANTITY__', $data['quantity']);
        define('__CATEGORIES__', $data['categories']);
        define('__IMAGES__', $data['images']);

        $response = "<h3>Produits importés:</h3><ul>";
        foreach ($csv as $row) {
            $product = new Product();

            if ($data['name'] != null) {
                $productName = $row[__NAME__];
            } else {
                $productName = "produit sans nom";
            }

            if ($data['price'] != null) {
                $productPrice = (float) $row[__PRICE__];
            } else {
                $productPrice = 0;
            }

            if ($data['description'] != null) {
                $productDescription = (float) $row[__DESCRIPTION__];
            } else {
                $productDescription = "Aucune description disponible";
            }

            if ($data['sku'] != null) {
                $productSku = $row[__SKU__];
            } else {
                $productSku = null;
            }

            if ($data['quantity'] != null) {
                $productQuantity = (int) $row[__QUANTITY__];
            } else {
                $productQuantity = null;
            }

            // $productCategories = $row[__CATEGORIES__];
            // $productImages = $row[__IMAGES__];

            $product->setName($productName);
            $product->setPrice($productPrice);
            $product->setDescription($productDescription);
            $product->setSku($productSku);
            $product->setQuantity($productQuantity);
            // categories to add
            // images to add
            $product->setUser($this->getUser());

            $slugify = new Slugify();
            $product->setSlug($this->getUser()->getId() . '-' . $slugify->slugify($product->getName()));

            $em->persist($product);

            $response .= "<li>$productName</li>";
        }
        $response .= "</ul>";
        $em->flush();

        $csv = $session->remove('csv');
        $data = $session->remove('data');

        return new Response($response, 200);
    }
}