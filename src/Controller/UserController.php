<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UpdatePasswordType;
use App\Form\UserMyProfileType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Cocur\Slugify\Slugify;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/dashboard/user")
 */
class UserController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/", name="dashboard_user_index", methods={"GET", "POST"})
     * @param Request $request
     * @param UserRepository $userRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, UserRepository $userRepository,  PaginatorInterface $paginator): Response
    {
        $users = $userRepository->findBy(["account_type" => "ROLE_PROVIDER"]);
        $users = $paginator->paginate(
            $users,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/my-profile", name="dashboard_user_my_profile", methods={"GET","POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function myProfile(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $originalLogo = $user->getLogo();
        $oldField = $user->getField();

        $form = $this->createForm(UserMyProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $logoFile = $form->get('logo')->getData();
            if ($logoFile) {
                $path = $this->getParameter('logos_directory');
                $originalFilename = pathinfo($logoFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logoFile->guessExtension();
                try {
                    $logoFile->move(
                        $path,
                        $newFilename
                    );
                } catch (FileException $e) {
                    $newFilename = "Error occured during uplaod";
                }
                $user->setLogo($newFilename);
            } else {
                $user->setLogo($originalLogo);
            }

            $slugify = new Slugify();
            $slug = $slugify->slugify($user->getName());
            $user->setSlug($slug);

            $field = $user->getField();
            if ($oldField != null) {
                if ($field->getId() != $oldField->getId()) {
                    $providersCount = $field->getProvidersCount();
                    $field->setProvidersCount(++$providersCount);

                    $providersCount = $oldField->getProvidersCount();
                    $oldField->setProvidersCount(--$providersCount);
                }
            } else {
                if ($field != null) {
                    $providersCount = $field->getProvidersCount();
                    $field->setProvidersCount(++$providersCount);
                }
            }

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Votre profil a été modifié avec succès !');
        }
        $em->refresh($user);

        return $this->render('user/my_profile.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/modifier-votre-mot-de-passe", name="dashboard_user_update_password", methods={"GET","POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(UpdatePasswordType::class, $user);
        $form->handleRequest($request);
        $currentPassword = $user->getPassword();

        if ($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $form->get('password')->getData();
            $newPassword = $form->get('plainPassword')->getData();

            if ($oldPassword == $currentPassword) {
                $newEncodedPassword = $passwordEncoder->encodePassword($user, $newPassword);
                $user->setPassword($newEncodedPassword);

                $this->addFlash('success', 'Mot de passe modifié avec succès !');
                $this->getDoctrine()->getManager()->flush();
            } else {
                $this->addFlash('success', "L'ancien mot de passe est incorrect");
            }
        }

        return $this->render('user/update_password.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/new", name="dashboard_user_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logoFile = $form->get('logo')->getData();
            if ($logoFile) {
                $path = $this->getParameter('logos_directory');
                $originalFilename = pathinfo($logoFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logoFile->guessExtension();
                try {
                    $logoFile->move(
                        $path,
                        $newFilename
                    );
                } catch (FileException $e) {
                    $newFilename = "Error occured during uplaod";
                }
                $user->setLogo($newFilename);
            }
            $user->setAccountType("ROLE_PROVIDER");
            $user->setRoles(["ROLE_PROVIDER"]);
            $user->setIsVerified(true);
            $slugify = new Slugify();
            $user->setSlug($slugify->slugify($user->getName()));

            /*$field = $user->getField();
            $providersCount = $field->getProvidersCount();
            $field->setProvidersCount(++$providersCount);*/

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard_user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}", name="dashboard_user_show", methods={"GET"})
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}/edit", name="dashboard_user_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function edit(Request $request, User $user): Response
    {
        $originalLogo = $user->getLogo();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logoFile = $form->get('logo')->getData();
            if ($logoFile) {
                $path = $this->getParameter('logos_directory');
                $originalFilename = pathinfo($logoFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logoFile->guessExtension();
                try {
                    $logoFile->move(
                        $path,
                        $newFilename
                    );
                } catch (FileException $e) {
                    $newFilename = "Error occured during uplaod";
                }
                $user->setLogo($newFilename);
            } else {
                $user->setLogo($originalLogo);
            }

            $slugify = new Slugify();
            $user->setSlug($slugify->slugify($user->getName()));

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dashboard_user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}", name="dashboard_user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dashboard_user_index');
    }
}
