<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\QuoteGeneration;
use App\Form\QuoteGenerationType;
use App\Repository\QuoteGenerationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/dashboard/quote/generation")
 */
class QuoteGenerationController extends AbstractController
{
    /**
     * @Route("/", name="dashboard_quote_generation_index", methods={"GET"})
     * @param Request $request
     * @param QuoteGenerationRepository $quoteGenerationRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, QuoteGenerationRepository $quoteGenerationRepository, PaginatorInterface $paginator): Response
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            $quote_generations = $quoteGenerationRepository->findAll();
        } else {
            $quote_generations = $quoteGenerationRepository->findBy(["provider" => $this->getUser()], ["id" => "DESC"]);
        }

        $quote_generations = $paginator->paginate(
            $quote_generations,
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('quote_generation/index.html.twig', [
            'quote_generations' => $quote_generations,
        ]);
    }

    /**
     * @Route("/new", name="dashboard_quote_generation_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $quoteGeneration = new QuoteGeneration();
        $form = $this->createForm(QuoteGenerationType::class, $quoteGeneration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($quoteGeneration);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard_quote_generation_index');
        }

        return $this->render('quote_generation/new.html.twig', [
            'quote_generation' => $quoteGeneration,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product-price", name="dashboard_get_product_price", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function getProductPrice(Request $request)
    {
        $data = $request->request->all();
        $id = (int) $data['id'];

        $product = $this->getDoctrine()->getManager()->getRepository(Product::class)->find($id);
        if ($product) {
            $price = $product->getPrice();
        } else {
            $price = 0;
        }

        return new Response($price, 200);
    }

    /**
     * @Route("/{id}", name="dashboard_quote_generation_show", methods={"GET"})
     * @param QuoteGeneration $quoteGeneration
     * @return Response
     */
    public function show(QuoteGeneration $quoteGeneration): Response
    {
        return $this->render('quote_generation/show.html.twig', [
            'quote_generation' => $quoteGeneration,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="dashboard_quote_generation_edit", methods={"GET","POST"})
     * @param Request $request
     * @param QuoteGeneration $quoteGeneration
     * @return Response
     */
    public function edit(Request $request, QuoteGeneration $quoteGeneration): Response
    {
        $form = $this->createForm(QuoteGenerationType::class, $quoteGeneration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $html = $this->getQuoteHtml($quoteGeneration);
            $quoteGeneration->setHtml($html);

            $amount = 0;
            $items = $quoteGeneration->getItems();
            foreach ($items as $item) {
                $product = $item->getProduct();
                $amount += $product->getPrice();
            }
            $quoteGeneration->setAmount($amount);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dashboard_quote_generation_index');
        }

        return $this->render('quote_generation/edit.html.twig', [
            'quote_generation' => $quoteGeneration,
            'form' => $form->createView(),
        ]);
    }

    private function getQuoteHtml(QuoteGeneration $quoteGeneration)
    {
        $em = $this->getDoctrine()->getManager();
        $html = '';
        $total = 0;
        $user = $quoteGeneration->getClient();
        $provider = $quoteGeneration->getProvider();
        $company = $quoteGeneration->getCompany();

        if ($company) {
            $clientName = $company->getName();
            $clientVat = $company->getVatNumber();
            $clientAddress = $company->getAddress();
        } else {
            $clientName = $user->getName();
            $clientVat = "-";
            $clientAddress = $user->getAddress();
        }

            // Header
            $imgSprindt = '/style/images/logo.png';
            $img = $provider->getLogoUrl();
            $date = new \DateTime();
            $date = $date->format('d/m/Y');

            $html .= '
            <table width="100%" class="nbd">
                <tr>
                    <td valign="middle" align="left">
                        <br><br>
                        <img src="' . $img . '" width="100" alt="logo" class="logo-provider">
                    </td>
                    <td valign="middle" align="right">
                        <table width="100%" class="nbd">
                            <tr>
                                <td valign="middle" align="right">
                                    <strong>Date</strong> : ' . $date . '
                                    <br>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="1" width="100%" cellpadding="5" class="bordered-custom-table">
                                        <tr>
                                            <td align="left">Nom/Raison sociale</td>
                                            <td align="center">' . $company->getName() . '</td>
                                        </tr>
                                        <tr>
                                            <td align="left">M.Fiscale</td>
                                            <td align="center">' . $company->getVatNumber() . '</td>
                                        </tr>
                                        <tr>
                                            <td align="left">Adresse</td>
                                            <td align="center">' . $company->getAddress() . '</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td valign="middle" align="right">
                        <br><br><br>
                        <h2 style="text-align: center">À l\'attention de</h2>
                        <br>
                        <table border="1" width="100%" cellpadding="5" class="bordered-custom-table">
                            <tr>
                                <td align="left">Nom</td>
                                <td align="center">' . $clientName . '</td>
                            </tr>
                            <tr>
                                <td align="left">M.Fiscale</td>
                                <td align="center">' . $clientVat . '</td>
                            </tr>
                            <tr>
                                <td align="left">Adresse</td>
                                <td align="center">' . $clientAddress . '</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br><br><br><br><br>
        ';

            $html .= '
            <table width="100%" class="nbd">
                <tr>
                    <td align="center">
                        <h2>Prototype de devis</h2>
                        <br>
                    </td>
                </tr>
            </table>';

            $html .= '<table border="1" width="100%" cellpadding="5" class="bordered-custom-table">';
            $html .= '
                <tr>
                    <td width="55%" valign="middle" align="left"><strong>DESCRIPTION</strong></td>
                    <td width="15%" valign="middle" align="right"><strong>QUANTITE</strong></td>
                    <td width="15%" valign="middle" align="right"><strong>P.U HT</strong></td>
                    <td width="15%" valign="middle" align="right"><strong>MONTANT HT</strong></td>
                </tr>
        ';

            $items = $quoteGeneration->getItems();
            foreach ($items as $item) {
                $product = $item->getProduct();
                $html .= '
                    <tr>
                        <td valign="top" align="left">' . $product->getDescription() . '</td>
                        <td valign="top" align="right">
                            ' . $item->getQuantity() . '
                            <br><br><br><br><br><br><br><br>
                        </td>
                        <td valign="top" align="right">' . $item->getUnitPrice() . '</td>';
                $productTotal = $item->getUnitPrice() * $item->getQuantity();
                $total += $productTotal;
                $html .= '<td valign="top" align="right">' . $productTotal . '</td>
                    </tr>
                ';
            }

            $html .= '</table>';

            $html .= '<table style="margin-top: -2px;" width="100.1%" class="nbd np">';
            $html .=
                '<tr>
                <td width="54.5%" class="td-w-55"></td>
                <td width="45%" class="td-n-p">
                    <table border="1" width="100%" cellpadding="5" class="total-table">
                        <tr>
                            <td width="66.7%" valign="middle" align="left">Sous-total</td>
                            <td width="33.3%" valign="middle" align="right">' . $total . '</td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left">Total TTC</td>
                            <td valign="middle" align="right">' . $total * 1.19 . '</td>
                        </tr>
                    </table>
                </td>
            </tr>
            ';

            $html .= '</table>';

            //footer
            $html .= '
            <br><br>
            <table width="100%" class="nbd">
                <tr>
                    <td align="center">
                        <br><br>
                        <strong>MERCI DE LA CONFIANCE QUE VOUS NOUS TÉMOIGNEZ</strong>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <br><br>
                        <strong>Signature</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br><br><br><br><br><br><br>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <hr>
                        <br><br>
                        Devis généré par la plateforme Sprindt<br>
                        <img style="width: 70px;" src="' . $imgSprindt . '" alt="" width="70">
                    </td>
                </tr>
            </table>
        ';

        return $html;
    }

    /**
     * @Route("/{id}", name="dashboard_quote_generation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, QuoteGeneration $quoteGeneration): Response
    {
        if ($this->isCsrfTokenValid('delete'.$quoteGeneration->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($quoteGeneration);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dashboard_quote_generation_index');
    }
}
