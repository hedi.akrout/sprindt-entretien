<?php
namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{

    /**
     * @Route("/ajouter-au-panier", name="front_add_to_cart")
     * @param Request $request
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function addToCart(Request $request, ProductRepository $productRepository)
    {
        $session = $request->getSession();
        $cart = $session->get('cart', false);
        $providerSlug = $session->get('provider', false);

        $data = $request->request->all();
        $id = $data['id'];

        if (isset($data['qty'])) {
            $qty = (int) $data['qty'];
        } else {
            $qty = 1;
        }

        if (!$cart) {
            $cart = [];
        }

        $product = $productRepository->findOneBy(['sp_sku' => $id]);
        if ($product) {
            if ($providerSlug != $product->getUser()->getSlug()) {
                $cart = [];
                $session->set('provider', $product->getUser()->getSlug());
            }
        }

        $alreadyExists = false;
        foreach ($cart as $cartProduct) {
            if ($cartProduct->id == $id) {
                $cartProduct->qty += $qty;
                $qty = $cartProduct->qty;
                $alreadyExists = true;
            }
        }

        if (!$alreadyExists) {
            $p = new \stdClass();
            $p->id = $id; $p->qty = $qty;
            $cart[] = $p;
            $session->set('cart', $cart);

            $html = '';
            if ($product) {
                if (!$product->getImages()->isEmpty()) {
                    $img_path = $product->getImages()[0]->getPublicPath();
                } else {
                    $img_path = "/style/images/placeholder.jpg";
                }
                $html = '
                <tr>
                    <td><img title="'.$product->getName().'" width="70" height="70" src="'.$img_path.'" alt="'.$product->getName().'"</td>
                    <td><p title="'.$product->getName().'">'.$product->getShortName().'</p></td>
                    <td class="p-r">
                        <input class="cart-control" type="number" value="'.$qty.'">
                        <a href="#" data-id="'.$product->getSpSku().'" class="badge badge-warning btn-update"><i class="jam jam-pen"></i></a>
                    </td>
                    <td><a href="#" data-id="'.$product->getSpSku().'" class="badge badge-danger btn-delete-product"><i class="jam jam-trash"></i></a></td>
                </tr>
            ';
            }
            $response = ["type" => "new", "html" => $html];
        } else {
            $response = ["type" => "old", "id" => $id, "qty" => $qty];
        }

        $response = json_encode($response);
        return new Response($response, 200);
    }

    /**
     * @Route("/supprimer-produit", name="front_remove_from_cart")
     * @param Request $request
     * @return Response
     */
    public function removeFromCart(Request $request)
    {
        $session = $request->getSession();
        $cart = $session->get('cart', false);

        $data = $request->request->all();
        $id = $data['id'];

        $i = 0;

        foreach ($cart as $cartProduct) {
            if ($cartProduct->id == $id) {
                unset($cart[$i]);
                $session->set('cart', $cart);
                return new Response("found", 200);
            } else {
                $i++;
            }
        }

        return new Response("not found", 200);
    }

    /**
     * @Route("/mettre-a-jour-quantite", name="front_update_quantity_cart")
     * @param Request $request
     * @return Response
     */
    public function updateQuantityCart(Request $request)
    {
        $session = $request->getSession();
        $cart = $session->get('cart', false);

        $data = $request->request->all();
        $id = $data['id'];
        $qty = (int) $data['qty'];

        $i = 0;

        foreach ($cart as $cartProduct) {
            if ($cartProduct->id == $id) {
                $cart[$i]->qty = $qty;
                $session->set('cart', $cart);
                return new Response("found", 200);
            } else {
                $i++;
            }
        }

        return new Response("not found", 200);
    }

    public function cartListing(Request $request)
    {
        $session = $request->getSession();
        $cart = $session->get('cart', false);
        $em = $this->getDoctrine()->getManager();

        $html = '';
        if ($cart ==  false) {
            $isEmptyCart = true;
        } else {
            $isEmptyCart = false;
            foreach ($cart as $item_id => $p) {
                $product = $em->getRepository(Product::class)->findOneBy(["sp_sku" => $p->id]);
                if ($product) {
                    if (!$product->getImages()->isEmpty()) {
                        $img_path = $product->getImages()[0]->getPublicPath();
                    } else {
                        $img_path = "/style/images/placeholder.jpg";
                    }
                    $html .= '
                        <tr>
                            <td><img title="'.$product->getName().'" width="70" height="70" src="'.$img_path.'" alt="'.$product->getName().'"</td>
                            <td><p title="'.$product->getName().'">'.$product->getShortName().'</p></td>
                            <td class="p-r">
                                <input data-product="'.$product->getSpSku().'" class="cart-control" type="number" value="'.$p->qty.'">
                                <a href="#" data-id="'.$product->getSpSku().'" class="badge badge-warning btn-update"><i class="jam jam-pen"></i></a>
                            </td>
                            <td><a href="#" data-id="'.$product->getSpSku().'" class="badge badge-danger btn-delete-product"><i class="jam jam-trash"></i></a></td>
                        </tr>
                    ';
                }
            }
        }

        return $this->render('cart/cart.html.twig', [
            'cart' => $cart,
            'isEmptyCart' => $isEmptyCart,
            'html' => $html
        ]);
    }
}