<?php
namespace App\Controller;

use App\Entity\Affiliation;
use App\Entity\Field;
use App\Form\AffiliationType;
use App\Repository\FieldRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{

    /**
     * @Route("/", name="front_home")
     * @param FieldRepository $fieldRepository
     * @return Response
     */
    public function home(FieldRepository $fieldRepository)
    {
        $fields = $fieldRepository->findBy([], ["providers_count" => "DESC"], 6);
        return $this->render("front/home.html.twig", [
            'fields' => $fields
        ]);
    }

    /**
     * @Route("/a-propos", name="front_about")
     * @return Response
     */
    public function about()
    {
        return $this->render("front/about.html.twig");
    }

    /**
     * @Route("/devenir-un-fournisseur-sur-sprindt", name="front_become_a_provider")
     * @return Response
     */
    public function becomeAProvider()
    {
        return $this->render("front/become_a_provider.html.twig");
    }

    /**
     * @Route("/demande-d-affiliation", name="front_apply_as_provider")
     * @return Response
     */
    public function applyAsProvider(Request $request)
    {
        $affiliation = new Affiliation();
        $form = $this->createForm(AffiliationType::class, $affiliation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($affiliation);
            $em->flush();
        }

        return $this->render("front/apply_as_a_provider.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/secteurs-activites", name="front_fields_index")
     * @param Request $request
     * @param FieldRepository $fieldRepository
     * @return Response
     */
    public function fields(Request $request, FieldRepository $fieldRepository)
    {
        return $this->render("front/field_index.html.twig", [
            'fields' => $fieldRepository->findAll()
        ]);
    }

    /**
     * @Route("/{slug}/fournisseurs", name="front_provider_index_from_field")
     * @param Request $request
     * @param $slug
     * @param FieldRepository $fieldRepository
     * @param PaginatorInterface $paginator
     * @param UserRepository $userRepository
     * @return Response
     */
    public function ProviderIndexFromField(Request $request, $slug, FieldRepository $fieldRepository,  PaginatorInterface $paginator, UserRepository $userRepository)
    {
        $field = $fieldRepository->findOneBy(['slug' => $slug]);
        if ($field) {
            $users = $userRepository->findByFields($field);
        } else {
            $users = [];
        }

        $users = $paginator->paginate(
            $users,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render("provider/index.html.twig", [
            'users' => $users
        ]);
    }
}