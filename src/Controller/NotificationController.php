<?php
namespace App\Controller;

use App\Repository\NotificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotificationController extends AbstractController
{

    public function listing(NotificationRepository $notificationRepository)
    {
        $user = $this->getUser();
        $notifications = $notificationRepository->findBy(["user" => $user, "is_seen" => false]);

        return $this->render("notifications/index.html.twig", [
            "notifications" => $notifications,
            "count"         => count($notifications)
        ]);
    }

    /**
     * @Route("/mark-notification-as-read", name="dashboard_mark_notification_as_read")
     * @param NotificationRepository $notificationRepository
     * @return Response
     */
    public function markNotificationsAsRead(NotificationRepository $notificationRepository): Response
    {
        $user = $this->getUser();
        $notifications = $notificationRepository->findBy(["user" => $user, "is_seen" => false]);
        $em = $this->getDoctrine()->getManager();
        foreach ($notifications as $notification) {
            $notification->setIsSeen(true);
            $em->flush();
        }

        return new Response('done', 200);
    }
}