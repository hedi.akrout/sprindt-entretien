<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findCount(User $user = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->select("COUNT(p.id)")
        ;

        if (null != $user && "ROLE_ADMIN" != $user->getAccountType()) {
            $qb->andWhere("p.user = :user")->setParameter(":user", $user);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getRelatedProducts(Product $product)
    {
        $category = $product->getCategories()->first();
        return $this->createQueryBuilder('p')
            ->leftJoin('p.categories', 'c')
            ->andWhere('c.parent = :parent_id')
            ->setParameter('parent_id', $category->getId())
            ->orWhere('c.id = :cat_id')
            ->setParameter('cat_id', $category->getId())
            ->andWhere('p.id != :current_p')
            ->setParameter('current_p', $product->getId())
            ->andWhere('p.user = :user')
            ->setParameter('user', $product->getUser())
            ->setMaxResults(4)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
