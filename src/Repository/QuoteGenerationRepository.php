<?php

namespace App\Repository;

use App\Entity\QuoteGeneration;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QuoteGeneration|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuoteGeneration|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuoteGeneration[]    findAll()
 * @method QuoteGeneration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuoteGenerationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuoteGeneration::class);
    }

    public function getMonthVolume(User $user = null, int $year = null)
    {
        $months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        $dates = [];

        if (!$year) {
            $year = (int) date('Y');
        }

        for ($i = 1; $i <= 12; $i++){
            $dates[] = date($year."-".$i);
        }

        $volume = [
            [
                'key' => "Les devis générés",
                'values' => []
            ],
            [
                'key' => "Les tickets ouverts",
                'values' => []
            ]
        ];

        foreach ($dates as $date) {
            $lastDay = date("t", strtotime("$date-1"));
            $dateGetter = new \DateTime($date);
            $monthIndex = (int) $dateGetter->format('m') - 1;

            // Quotes generations count
            $generatedQuotes = $this->createQueryBuilder('q')
                ->select('COUNT(q.id)')
                ->andWhere('q.created_at >= :date_from')
                ->andWhere('q.created_at <= :date_to')
                ->setParameter('date_from', $date.'-1 00:00:00')
                ->setParameter('date_to', $date.'-'.$lastDay.' 23:59:59')
            ;
            if ($user != null && $user->getAccountType() == "ROLE_PROVIDER") {
                $generatedQuotes = $generatedQuotes
                    ->andWhere('q.provider = :user')
                    ->setParameter('user', $user)
                ;
            }
            $generatedQuotes = $generatedQuotes
                ->getQuery()
                ->getSingleScalarResult()
            ;
            $volume[0]['values'][] = ["date" => $months[$monthIndex], "count" => $generatedQuotes];

            // Tickets count
            $generatedQuotes = $this->createQueryBuilder('q')
                ->select('COUNT(q.id)')
                ->andWhere('q.created_at >= :date_from')
                ->andWhere('q.created_at <= :date_to')
                ->andWhere('q.is_ordered = true')
                ->setParameter('date_from', $date.'-1 00:00:00')
                ->setParameter('date_to', $date.'-'.$lastDay.' 23:59:59')
           ;
            if ($user != null && $user->getAccountType() == "ROLE_PROVIDER") {
                $generatedQuotes = $generatedQuotes
                    ->andWhere('q.provider = :user')
                    ->setParameter('user', $user)
                ;
            }
            $generatedQuotes = $generatedQuotes
                ->getQuery()
                ->getSingleScalarResult()
            ;
            $volume[1]['values'][] = ["date" => $months[$monthIndex], "count" => $generatedQuotes];
        }

        return $volume;
    }

    public function getMonthCount(User $user = null)
    {
        $month = date('m');
        $year = date('Y');
        $lastDay = date("t", strtotime("$year-$month-1"));

        $qb = $this->createQueryBuilder('q')
            ->select('COUNT(q.id)')
            ->andWhere('q.created_at >= :date_from')
            ->andWhere('q.created_at <= :date_to')
            ->setParameter('date_from', date('Y-'.$month.'-1')." 00:00:00")
            ->setParameter('date_to', date('Y-'.$month.'-'.$lastDay)." 23:59:59")
        ;
        if ($user) {
            $qb->andWhere('q.user = :user')->setParameter('user', $user);
        }
        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getMonthAmount(User $user = null, $from = null, $to = null)
    {
        if ($from != null || $to != null) {
            $start_date = $from." 00:00:00";
            $end_date = $to." 23:59:59";
        } else {
            $month = date('m');
            $year = date('Y');
            $lastDay = date("t", strtotime("$year-$month-1"));

            $start_date = date('Y-'.$month.'-1')." 00:00:00";
            $end_date = date('Y-'.$month.'-'.$lastDay)." 23:59:59";
        }

        $qb = $this->createQueryBuilder('q')
            ->select('SUM(q.amount) as sum, q.is_ordered')
            ->andWhere('q.created_at >= :start_date')
            ->andWhere('q.created_at <= :end_date')
            ->setParameter('start_date', $start_date)
            ->setParameter('end_date', $end_date)
            ->groupBy("q.is_ordered")
        ;
        if ($user) {
            $qb = $qb->andWhere('q.user = :user')->setParameter('user', $user);
        }
        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return QuoteGeneration[] Returns an array of QuoteGeneration objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuoteGeneration
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
