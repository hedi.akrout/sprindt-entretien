<?php

namespace App\Repository;

use App\Entity\Ticket;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ticket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ticket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ticket[]    findAll()
 * @method Ticket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    public function getMonthCount()
    {
        $month = date('m');
        $year = date('Y');
        $lastDay = date("t", strtotime("$year-$month-1"));

        return $this->createQueryBuilder('q')
            ->select('COUNT(q.id)')
            ->andWhere('q.created_at >= :date_from')
            ->andWhere('q.created_at <= :date_to')
            ->setParameter('date_from', date('Y-'.$month.'-1')." 00:00:00")
            ->setParameter('date_to', date('Y-'.$month.'-'.$lastDay)." 23:59:59")
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function getMonthState(User $user = null, $from = null, $to = null)
    {
        if ($from != null || $to != null) {
            $start_date = $from." 00:00:00";
            $end_date = $to." 23:59:59";
        } else {
            $month = date('m');
            $year = date('Y');
            $lastDay = date("t", strtotime("$year-$month-1"));

            $start_date = date('Y-'.$month.'-1')." 00:00:00";
            $end_date = date('Y-'.$month.'-'.$lastDay)." 23:59:59";
        }

        $qb = $this->createQueryBuilder('q')
            ->select('q.state as state, COUNT(q.id) as count')
            ->andWhere('q.created_at >= :date_from')
            ->andWhere('q.created_at <= :date_to')
            ->setParameter('date_from', $start_date)
            ->setParameter('date_to', $end_date)
        ;
        if ($user != null) {
            $qb = $qb->andWhere('q.user = :user')->setParameter('user', $user);
        }

        $result = $qb->groupBy('q.state')
            ->getQuery()
            ->getResult()
        ;

        $states = [
            '0' => 0,
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
        ];
        foreach ($result as $entry) {
            $states[$entry['state']] = $entry['count'];
        }
        return $states;
    }

    // /**
    //  * @return Ticket[] Returns an array of Ticket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ticket
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
