<?php

namespace App\Entity;

class PropertySearch
{
    /**
     * @var string|null
     */
    private $name;
    /**
     * @var string|null
     */
    private $region;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return PropertySearch
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     * @return PropertySearch
     */
    public function setRegion(?string $region): PropertySearch
    {
        $this->region = $region;
        return $this;
    }
}