<?php

namespace App\Entity;

use App\Repository\QuoteGenerationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuoteGenerationRepository::class)
 * @ORM\Table(name="sp_quote_generation")
 */
class QuoteGeneration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $html;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_ordered;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToOne(targetEntity=Ticket::class, inversedBy="quoteGeneration", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="ticket_id", referencedColumnName="id", nullable=true)
     */
    private $ticket;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="generation_from")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="generation_to")
     */
    private $provider;

    /**
     * @ORM\OneToMany(targetEntity=QuoteItem::class, mappedBy="quote_generation", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $items;

    /**
     * @ORM\Column(type="float")
     */
    private $amount = 0;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="quoteGenerations")
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }

    public function setHtml(string $html): self
    {
        $this->html = $html;

        return $this;
    }

    public function getIsOrdered(): ?bool
    {
        return $this->is_ordered;
    }

    public function setIsOrdered(bool $is_ordered): self
    {
        $this->is_ordered = $is_ordered;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setTicket(?Ticket $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getProvider(): ?User
    {
        return $this->provider;
    }

    public function setProvider(?User $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function __construct()
    {
        $this->is_ordered = false;
        $this->created_at = new \DateTime();
        $this->items = new ArrayCollection();
    }

    /**
     * @return Collection|QuoteItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(QuoteItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setQuoteGeneration($this);
        }

        return $this;
    }

    public function removeItem(QuoteItem $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getQuoteGeneration() === $this) {
                $item->setQuoteGeneration(null);
            }
        }

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
