<?php

namespace App\Entity;

use App\Repository\FieldRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FieldRepository::class)
 * @ORM\Table(name="sp_field")
 */
class Field
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $providers_count;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="fields")
     */
    private $users;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getProvidersCount(): ?string
    {
        return $this->providers_count;
    }

    public function setProvidersCount(string $providers_count): self
    {
        $this->providers_count = $providers_count;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param mixed $providers
     * @return Field
     */
    public function setProviders($providers): self
    {
        $this->providers = $providers;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->providers_count = 0;
        $this->users = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Field
     */
    public function setImage($image): Field
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addField($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeField($this);
        }

        return $this;
    }
}
