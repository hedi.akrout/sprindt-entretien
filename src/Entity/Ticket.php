<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TicketRepository::class)
 * @ORM\Table(name="sp_ticket")
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $state;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $htmlQuote;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ticketsFor")
     *
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ticketsFrom")
     *
     */
    private $client;

    /**
     * @ORM\OneToOne(targetEntity=QuoteGeneration::class, mappedBy="ticket", cascade={"persist", "remove"})
     */
    private $quoteGeneration;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->state = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getHtmlQuote(): ?string
    {
        return $this->htmlQuote;
    }

    public function setHtmlQuote(?string $htmlQuote): self
    {
        $this->htmlQuote = $htmlQuote;

        return $this;
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function setProvider($provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient($client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Ticket
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    public function getQuoteGeneration(): ?QuoteGeneration
    {
        return $this->quoteGeneration;
    }

    public function setQuoteGeneration(?QuoteGeneration $quoteGeneration): self
    {
        // unset the owning side of the relation if necessary
        if ($quoteGeneration === null && $this->quoteGeneration !== null) {
            $this->quoteGeneration->setTicket(null);
        }

        // set the owning side of the relation if necessary
        if ($quoteGeneration !== null && $quoteGeneration->getTicket() !== $this) {
            $quoteGeneration->setTicket($this);
        }

        $this->quoteGeneration = $quoteGeneration;

        return $this;
    }
}
