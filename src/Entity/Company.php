<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 * @ORM\Table(name="sp_company")
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="companies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $generations_count = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $vat_number;

    /**
     * @ORM\OneToMany(targetEntity=QuoteGeneration::class, mappedBy="company")
     */
    private $quoteGenerations;

    public function __construct()
    {
        $this->quoteGenerations = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getGenerationsCount(): ?int
    {
        return $this->generations_count;
    }

    public function setGenerationsCount(int $generations_count): self
    {
        $this->generations_count = $generations_count;

        return $this;
    }

    public function getVatNumber(): ?string
    {
        return $this->vat_number;
    }

    public function setVatNumber(string $vat_number): self
    {
        $this->vat_number = $vat_number;

        return $this;
    }

    /**
     * @return Collection|QuoteGeneration[]
     */
    public function getQuoteGenerations(): Collection
    {
        return $this->quoteGenerations;
    }

    public function addQuoteGeneration(QuoteGeneration $quoteGeneration): self
    {
        if (!$this->quoteGenerations->contains($quoteGeneration)) {
            $this->quoteGenerations[] = $quoteGeneration;
            $quoteGeneration->setCompany($this);
        }

        return $this;
    }

    public function removeQuoteGeneration(QuoteGeneration $quoteGeneration): self
    {
        if ($this->quoteGenerations->removeElement($quoteGeneration)) {
            // set the owning side to null (unless already changed)
            if ($quoteGeneration->getCompany() === $this) {
                $quoteGeneration->setCompany(null);
            }
        }

        return $this;
    }
}
