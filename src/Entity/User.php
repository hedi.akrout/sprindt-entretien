<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="sp_user")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var String
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vat_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $account_type;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="user", cascade={"persist", "remove"})
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="Ticket", mappedBy="client", cascade={"persist", "remove"})
     */
    private $ticketsFrom;

    /**
     * @ORM\OneToMany(targetEntity="Ticket", mappedBy="provider", cascade={"persist", "remove"})
     */
    private $ticketsFor;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $address;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\OneToMany(targetEntity=QuoteGeneration::class, mappedBy="client")
     */
    private $generation_from;

    /**
     * @ORM\OneToMany(targetEntity=QuoteGeneration::class, mappedBy="provider")
     */
    private $generation_to;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_subscribed = false;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="user")
     */
    private $notifications;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity=Company::class, mappedBy="user")
     * @Assert\Count(max=3, maxMessage="Vous ne pouvez pas spécifier plus de {{ limit }} entreprises")
     */
    private $companies;

    /**
     * @ORM\Column(type="array")
     */
    private $regions = [];

    /**
     * @ORM\ManyToMany(targetEntity=Field::class, inversedBy="users")
     */
    private $fields;

    /**
     * @return mixed
     */
    public function getTicketsFrom()
    {
        return $this->ticketsFrom;
    }

    /**
     * @param mixed $ticketsFrom
     * @return User
     */
    public function setTicketsFrom($ticketsFrom)
    {
        $this->ticketsFrom = $ticketsFrom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTicketsFor()
    {
        return $this->ticketsFor;
    }

    /**
     * @param mixed $ticketsFor
     * @return User
     */
    public function setTicketsFor($ticketsFor)
    {
        $this->ticketsFor = $ticketsFor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVatNumber()
    {
        return $this->vat_number;
    }

    /**
     * @param mixed $vat_number
     * @return User
     */
    public function setVatNumber($vat_number)
    {
        $this->vat_number = $vat_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return User
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function __construct()
    {
        $this->generation_from = new ArrayCollection();
        $this->generation_to = new ArrayCollection();
        $this->notifications = new ArrayCollection();

        $this->created_at = new \DateTime();
        $this->companies = new ArrayCollection();
        $this->fields = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     * @return User
     */
    public function setLogo($logo): User
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    public function getLogoUrl()
    {
        return '/logos/'.$this->logo;
    }

    /**
     * @param mixed $products
     * @return User
     */
    public function setProducts($products): User
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountType()
    {
        return $this->account_type;
    }

    /**
     * @param mixed $account_type
     * @return User
     */
    public function setAccountType($account_type): User
    {
        $this->account_type = $account_type;
        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|QuoteGeneration[]
     */
    public function getGenerationFrom(): Collection
    {
        return $this->generation_from;
    }

    public function addGenerationFrom(QuoteGeneration $generationFrom): self
    {
        if (!$this->generation_from->contains($generationFrom)) {
            $this->generation_from[] = $generationFrom;
            $generationFrom->setClient($this);
        }

        return $this;
    }

    public function removeGenerationFrom(QuoteGeneration $generationFrom): self
    {
        if ($this->generation_from->removeElement($generationFrom)) {
            // set the owning side to null (unless already changed)
            if ($generationFrom->getClient() === $this) {
                $generationFrom->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|QuoteGeneration[]
     */
    public function getGenerationTo(): Collection
    {
        return $this->generation_to;
    }

    public function addGenerationTo(QuoteGeneration $generationTo): self
    {
        if (!$this->generation_to->contains($generationTo)) {
            $this->generation_to[] = $generationTo;
            $generationTo->setProvider($this);
        }

        return $this;
    }

    public function removeGenerationTo(QuoteGeneration $generationTo): self
    {
        if ($this->generation_to->removeElement($generationTo)) {
            // set the owning side to null (unless already changed)
            if ($generationTo->getProvider() === $this) {
                $generationTo->setProvider(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return User
     */
    public function setAddress($address): self
    {
        $this->address = $address;
        return $this;
    }

    public function getIsSubscribed(): ?bool
    {
        return $this->is_subscribed;
    }

    public function isSubscribed(): ?bool
    {
        return $this->is_subscribed;
    }

    public function setIsSubscribed(bool $is_subscribed): self
    {
        $this->is_subscribed = $is_subscribed;

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setUser($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getUser() === $this) {
                $notification->setUser(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->setUser($this);
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->companies->removeElement($company)) {
            // set the owning side to null (unless already changed)
            if ($company->getUser() === $this) {
                $company->setUser(null);
            }
        }

        return $this;
    }

    public function getRegions(): ?array
    {
        return $this->regions;
    }

    public function setRegions(array $regions): self
    {
        $this->regions = $regions;

        return $this;
    }

    /**
     * @return Collection|Field[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    public function addField(Field $field): self
    {
        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
        }

        return $this;
    }

    public function removeField(Field $field): self
    {
        $this->fields->removeElement($field);

        return $this;
    }
}
