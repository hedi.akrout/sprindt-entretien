<?php

namespace App\Entity;

use App\Repository\QuoteItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuoteItemRepository::class)
 * @ORM\Table(name="sp_quote_item")
 */
class QuoteItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="quoteItems")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=QuoteGeneration::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $quote_generation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $unit_price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $quantity;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuoteGeneration(): ?QuoteGeneration
    {
        return $this->quote_generation;
    }

    public function setQuoteGeneration(?QuoteGeneration $quote_generation): self
    {
        $this->quote_generation = $quote_generation;

        return $this;
    }

    public function getUnitPrice(): ?string
    {
        return $this->unit_price;
    }

    public function setUnitPrice(string $unit_price): self
    {
        $this->unit_price = $unit_price;

        return $this;
    }

    public function __toString(): string
    {
        return $this->product->getName();
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
