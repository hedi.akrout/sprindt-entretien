https://gitlab.com/hedi.akrout/sprindt-entretien

NB: J'ai ajouté un fichier sprindt.sql contenant le script pour l'import de la base de données (En plus du chargement des fixtures)

===Front:
**Username: commercial@hp.com
**Password: demo

Les fournisseurs ayant des produits à tester: TunisiaNet dans la catégorie Informatique.


===Back:
Url: /dashboard/
**Username: admi@email.com
**Password: demo

Fonctionnalités à trouver: 
- Statistiques des ventes
- CRUD produits
- Import produits depuis un fichier CSV
- Système de réponse des tickets
- CRUD utilisateurs
- Consultation des demandes des devis/achats


===Scénarios
En tant que client, je collecte les produits désirés du fournisseurs, je demande la génération mon devis (authentification requise), je sélectionne pour laquelle de mes entreprise je génère le devis et je valide en ouvrant un ticket si je souhaite procéder à l'achat.

En tant qu'admin (ou fournisseur pour la partie des produits), je peux ajouter/modifier/supprimer mes produits, utilisateurs (fournisseurs), gérer mes tickets, consulter mes notifications et voir les statistiques.